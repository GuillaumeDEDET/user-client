'use strict';

const async = require('async');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const envConfig = require('../environments/all');

module.exports.init = server => {
    return new Promise((resolve, reject) => {
        async.series({
            good(done) {
                server.register({
                    register: require('good')
                }, done);
            },
            blipp(done) {
                server.register({
                    register: require('blipp'),
                    options: {
                        showStart: envConfig.log.showRouteAtStart,
                        showAuth: true
                    }
                }, done);
            },
            swagger(done) {
                const options = {
                    info: {
                        'title': 'Main project API Documentation',
                        'version': '1.0',
                    }
                };
                server.register([
                    Inert,
                    Vision,
                    {
                        'register': HapiSwagger,
                        'options': options
                    }], done);
            },
            boom(done) {
                server.register({
                    register: require('hapi-boom-decorators'),
                }, done);
            },
            ioClient(done){
                server.register({
                    register : require('../../app/plugins/ioClient'),
                    options : {
                        server : envConfig.socketIo.server
                    }
                }, done);
            }
        }, err => {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
};