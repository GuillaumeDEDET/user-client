'use strict';
const fs = require('fs');
const path = require('path');
const modelsDir = path.join(__dirname, '../../app/models/');
const models = fs.readdirSync(modelsDir);


module.exports.init = server => {
    return new Promise((resolve, reject) => {
        const connection = server.app.envs.connections.db;

        server.register({
            register: require('k7'),
            options: {
                connectionString: connection.type + '://' + connection.ip + ':' + connection.port + '/' + connection.database,
                adapter: require('k7-mongoose'),
                models: [
                    path.join(modelsDir, '**/*.js')
                ],
            }
        }, err => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        });
    });
};