'use strict';

const Faker = require('faker');
const RandomString = require("randomstring");
const _ = require('lodash');
const bcrypt = require('bcrypt');
const boom = require('boom');

module.exports.getAllUsers = (request,reply) => {

    let users =  request.server.database.user;

    users.find({}, function(err, users) {
        if (err) {
            reply.boom(500, err, "error Mongo");
            throw err;
        }

        users = users.map(user => user.toObject());
        reply(null, users);
    });
};

module.exports.getUser = (request, reply) => {

    let user = request.server.database.user;

    user.findOne({_id: request.params._id}, (err, user) => {
        if (err) {
            reply.boom(500, err, "error");
            return;
        }

        if (!user) {
            reply.notFound('user not found');
            return;
        }

        reply(null, user.toObject());
    });
};

module.exports.createUser = (request, reply) => {

    let user = request.server.database.user();

    user.set(request.payload);

    user.save((err) => {
        if (err) {
            reply.boom(500, err, "error Mongo");
            return;
        }
        request.server.ioClient.emit('mail-creation', { emailAddress: user.email, name: user.firstname + ' ' + user.lastname,  login: user.login, password: request.payload.password });
        reply(null, user.toObject());
    });
};



module.exports.updateUser = (request, reply) => {
    let user = request.server.database.user;

    if(request.payload.password != undefined){
        request.payload.password = bcrypt.hashSync(request.payload.password, 10);
    }

    user.findOneAndUpdate({_id: request.params._id},request.payload)
        .then(
            (data) => {
                if (request.payload.password != undefined || request.payload.login != undefined) {
                    request.server.ioClient.emit('mail-edition', { emailAddress: data.email, name: data.firstname + ' ' + data.lastname});
                }
                reply({response:"user updated"}).code(200);
            })
        .catch(
            (err) => {
                reply.boom(500, err, "error Mongo");
            }
        );
};

module.exports.deleteUser = (request, reply) => {
    let user = request.server.database.user;

    user.findOneAndRemove({_id: request.params._id}, (err, user) => {
        if (err) {
            reply.boom(500, err, "error Mongo");
            return;
        }

        reply({response:"user deleted"}).code(204);
    });
};

module.exports.authenticateUser = (request, reply) => {
    let user = request.server.database.user;

    user.findOne({'login': request.payload.login}, (err, user) => {
        if(err) {
            reply.boom(500, err, "error Mongo");
            return;
        }

        bcrypt.compare(request.payload.password, user.password,function(err,isMatch){
            if(err) throw err;
            isMatch ? reply({response:"ok"}).code(200) : reply(boom.unauthorized("ko"));
        })
    });

};

module.exports.resetPassword = (request, reply) => {
    let user = request.server.database.user;

    user.findOne({'email': request.payload.email}, (err, user) => {
        if (err) {
            reply.boom(500, err, 'error Mongo');
            return;
        }

        if (!user) {
            reply.notFound('user not found');
            return;
        }

        let newPassword = RandomString.generate(10);
        user.set({'password': newPassword});
        user.save((err, user) => {
            request.server.ioClient.emit('mail-reset', { emailAddress: user.email, name: user.firstname + ' ' + user.lastname,  login: user.login, password: newPassword });
            reply(null, {'msg': 'New password sended'});
        });

    });
};



module.exports.generateUsers = (request, reply) => {

    let users = [];

    for (let i = 0; i <= parseInt(request.params.number); i++) {
        let user = new request.server.database.user();
        user.set({
            login: Faker.internet.userName(),
            password: Faker.internet.password(),
            email: Faker.internet.email(),
            firstname: Faker.name.firstName(),
            lastname: Faker.name.lastName()
        });
        users.push(user.save());
    }

    Promise.all(users).then(users => {
        reply(null, _.map(users, (user) => {
            return user.toObject();
        }));
    }).catch(err => {
        reply.badImplementation(err);
    });
};
