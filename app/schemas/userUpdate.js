
const Joi = require('joi');

let schema = Joi.object().keys({
    login: Joi.string(),
    password: Joi.string().min(8),
    email: Joi.string().email(),
    firstname: Joi.string(),
    lastname: Joi.string(),
    nir: Joi.string().length(15),
});

module.exports = schema;