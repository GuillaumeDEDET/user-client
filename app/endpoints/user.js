'use strict';

const handler = require('../handlers/user');
const schema = require('../schemas/user');
const schemaUpdate = require('../schemas/userUpdate');
const Joi = require('joi');

exports.register = (server, options, next) => {
    server.route([
        {
            method  : 'GET',
            path    : '/users/',
            config  : {
                description : 'Liste des utilisateurs',
                notes       : 'Route qui affiche la liste des utilisateurs',
                tags        : ['api'],
                handler     : handler.getAllUsers
            }
        },
        {
            method : 'GET',
            path   : '/user/{_id}',
            config : {
                description : 'Affiche un utilisateur',
                notes       : 'Route qui affiche un utilisateur via son id',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.getUser,
                validate: {
                    params: {
                        _id: Joi.string().required()
                    }
                }
            }
        },
        {
            method : 'POST',
            path   : '/user',
            config : {
                description : 'Creation utilisateur',
                notes       : 'Route de création d\'un utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.createUser,
                validate: {
                    payload: schema
                }
            }
        },
        {
            method : 'PUT',
            path   : '/user/{_id}',
            config : {
                description : 'Mis à jour d\'un utilisateur',
                notes       : 'Route de mise à jour d\'un utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.updateUser,
                validate: {
                    payload: schemaUpdate,
                    params: {
                        _id: Joi.string().required()
                    }
                },
            }
        },
        {
            method : 'DELETE',
            path   : '/user/{_id}',
            config : {
                description : 'Supprime un utilisateur',
                notes       : 'Route qui permet de supprimer un utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.deleteUser,
                validate: {
                    params: {
                        _id: Joi.string().required()
                    }
                },
            }
        },
        {
            method : 'POST',
            path   : '/authent',
            config : {
                description : 'Login d\'utilisateur',
                notes       : 'Route qui permet à un utilisateur de s\'authentifier',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.authenticateUser,
                validate: {
                    payload: {
                        login: Joi.string(),
                        password: Joi.string()
                    }
                },
            }
        },
        {
            method : 'PATCH',
            path   : '/user/reset',
            config : {
                description : 'Envoi d\'un nouveau mot de passe',
                notes       : 'Route qui permet d\'envoyer un nouveau mot de passe a l\'utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.resetPassword,
                validate: {
                    payload: {
                        email: Joi.string().email()
                    }
                },
            }
        },
        {
            method : 'POST',
            path   : '/user/generate/{number}',
            config : {
                description : 'Créer certain un nombre d\'utilisateur',
                notes       : 'Route qui permet de créer un certain nombre d\'utilisateur',
                tags        : [ 'api' ],
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                handler     : handler.generateUsers,
                validate: {
                    params: {
                        number: Joi.number().integer().max(100)
                    }
                },
            }
        }
    ]);
    next();
};

exports.register.attributes = {
    name : 'user-routes'
};

